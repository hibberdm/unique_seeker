#!/usr/bin/env python3

import argparse
import sys
import os
import subprocess
import shutil
import hashlib

from Bio import SeqIO
from Bio import Seq

def report_call(script, args_namespace):
    sys.stdout.write("Script: {}\nVariables:\n".format(script))
    for arg in vars(args_namespace):
        print("    [{}]: {}".format(arg, getattr(args, arg)))
    sys.stdout.write("\n")


def gb_to_fna(genome_dir, gb_file):
    input_handle  = open(os.path.join(genome_dir, gb_file), "r")
    fna_file = os.path.splitext(gb_file)[0] + ".fna"
    output_handle = open(os.path.join(genome_dir, fna_file), "w")
    sys.stdout.write("[GB_TO_FNA] Converting {} to {} in {}".format(gb_file, fna_file, genome_dir))
    sys.stdout.flush()
    for seq_record in SeqIO.parse(input_handle, "genbank") :
        output_handle.write(">{} {}\n{}\n".format(seq_record.id, seq_record.description, str(seq_record.seq)))
    input_handle.close()
    output_handle.close()

def fna_to_kmers(genome_dir, fna_file, kmer_length):
    input_handle  = open(os.path.join(genome_dir, fna_file), "r")
    kmer_file = os.path.join(genome_dir, os.path.splitext(fna_file)[0] + "_" + kmer_length + "bp.kmers")
#    sys.stdout.write("In function: {}\n".format(kmer_file))
    if not os.path.isfile(kmer_file):
        sys.stdout.write("[FNA_TO_KMER] Creating tiled kmer file: " + kmer_file + "\n")
        sys.stdout.flush()
        output_handle = open(kmer_file, "w")
        for seq_record in SeqIO.parse(input_handle, "fasta") :
            for start in range(0, len(seq_record)-int(kmer_length), 1):
                end = start + int(kmer_length)
                if not seq_record.seq[start:end].count("N") > 0:
                    output_handle.write(">{}\n{}\n".format(os.path.splitext(fna_file)[0] + "_" + seq_record.id + "_" + str(start) + "_" + str(end), str(seq_record.seq[start:end])))
        input_handle.close()
        output_handle.close()
        return kmer_file
    else:
        sys.stdout.write("[FNA_TO_KMER] {} already exists, skipping...\n".format(kmer_file))
        sys.stdout.flush()
        return kmer_file

def create_database(genome_dir, genome_set, db_path):
    if not os.path.isfile(db_path):
        sys.stdout.write("[CREATE_DATABASE] Creating database: " + db_path + "\n")
        handle = open(db_path, "a")
        for genome in genome_set:
    		#                    print("{}\n".format(genome))
            for record in SeqIO.parse(os.path.join(args.genome_dir, genome), "fasta"):
                record.description += " | " + os.path.splitext(genome)[0]
                SeqIO.write(record, handle, "fasta")
        handle.close()
    else:
        sys.stdout.write("[CREATE_DATABASE] Existing database for " + db_path + " found!\n")
    sys.stdout.flush()

def index_database(db_path):
    if not os.path.isfile(db_path + ".1.ebwt"):
        sys.stdout.write("[INDEX_DATABASE] Creating bowtie index for " + db_path + "\n")
        subprocess.call(['bowtie-build', '-q', db_path, db_path])
    else:
        sys.stdout.write("[INDEX_DATABASE] Existing bowtie index for " + db_path + " found!\n")
    sys.stdout.flush()


def check_command(command):
    if not (shutil.which(command) is not None):
    	sys.exit("[ERROR] " + command + " is not available!")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mapping_file", help="[STRING] Input mapping file", dest="mapping_file", required=True)
    parser.add_argument("-g", "--genomes_directory", help="[STRING] Genome directory", dest="genome_dir", required=True)
    parser.add_argument("-k", "--kmers", help="[STRING] kmers list (single or comma-separated)", dest="kmer_list", required=True)
    parser.add_argument("-u", "--unique_keep", help="[FLAG] Enable collection of unique reads (comma-separated)", dest="retain_unique", action="store_true")
    parser.add_argument("-n", "--number_mismatches", help="[INTEGER] Allowable mismatches in mapping", dest="mismatches", default="0")
    args = parser.parse_args()

    report_call(sys.argv[0], args)

    check_command("bowtie-build")
    check_command("samtools")

    kmer_array = sorted(args.kmer_list.split(','))

    mapping_lines = open( args.mapping_file, 'r').readlines()
    first = True
    headers = list()
    databases = dict()
    mapping_jobs = set()
#    counting_jobs = dict()
    suffixes = set()
    for line in mapping_lines:
        if line.startswith('#'):
            continue
        else:
            stripline = line.strip()
            splitline = stripline.split('\t')
            if (first):
                headers = splitline
                first = False
            else:
                sample_id = splitline[headers.index('SID')]
                genome_string = splitline[headers.index('GENOMES')]
                genome_set = genome_string.split(',')
#                genome_set_sort = sorted(genome_set.split(','))
                genome_set_format = set()

                for genome in genome_set:
                    if not (os.path.isfile(os.path.join(args.genome_dir, genome))):
                        sys.exit("[ERROR] Genome not found: " + genome)
                    if os.path.splitext(genome)[1] == ".gb" or os.path.splitext(genome)[1] == ".gbk":
                        fna_file = gb_to_fna(genome_dir, genome)
                        genome_set_format.add(fna_file)
                    elif os.path.splitext(genome)[1] == ".fna":
                        genome_set_format.add(genome)
                    else:
                        sys.exit("[ERROR] Incompatible file format detected for: " + genome)
    				# suffixes.add(os.path.splitext(genome)[1])
    #				sys.stdout.write("Genome: {}".format(genome))
    				# if not args.rsem_flag:
    				# 	if not genome in counting_jobs:
    				# 		counting_jobs[genome] = set()
    				# 		counting_jobs[genome].add(mapped_dir + sample_id + ".bam")
    				# 	else:
    				# 		counting_jobs[genome].add(mapped_dir + sample_id + ".bam")
                db_name = ','.join(sorted(genome_set_format))
#                sys.stdout.write(db_name + "\n")
                db_hash = hashlib.md5(db_name.encode('utf-8')).hexdigest()
#                sys.stdout.write(db_hash + "\n")
#                sys.stdout.flush()

                if not (genome_string in databases):
                    databases[genome_string] = db_hash
                    db_path = os.path.join(args.genome_dir, db_hash + ".fna")
                    igs_path = os.path.join(args.genome_dir, db_hash + ".igs")

                    create_database(args.genome_dir, genome_set_format, db_path)
                    index_database(db_path)

                    if not os.path.isfile(igs_path):
                        sys.stdout.write("[STATUS] Creating tiled kmer files for genomes in database: " + db_path + "\n")
                        sys.stdout.flush()
                        for genome in genome_set_format:
                            for kmer_length in kmer_array:
                                kmer_file = fna_to_kmers(args.genome_dir, genome, kmer_length)
#                                sys.stdout.write("After return: {}\n".format(kmer_file))
                                output_file_base = os.path.splitext(kmer_file)[0] + "_" + db_hash
                                if (args.retain_unique):
                                    mapping_jobs.add(db_path + "\t" + kmer_file + "\t" + output_file_base + "\t" + "KEEP_UNIQUE" + "\t" + args.mismatches + "\n")
                                else:
                                    mapping_jobs.add(db_path + "\t" + kmer_file + "\t" + output_file_base + "\t" + "DISCARD_UNIQUE" + "\t" + args.mismatches + "\n")
                    else:
                        sys.stdout.write("[STATUS] Existing IGS table found for " + db_path + " found!\n")
                        sys.stdout.flush()

mapping_jobs_file = open("igs_mapping_tasks.job", 'w')

for mapping_job in sorted(mapping_jobs):
	mapping_jobs_file.write(mapping_job)

mapping_jobs_file.close()
