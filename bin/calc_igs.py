#!/usr/bin/env python3

import argparse
import sys
import os
import subprocess
import shutil
import hashlib
import pysam

from Bio import SeqIO
from Bio import Seq

def report_call(script, args_namespace):
    sys.stdout.write("Script: {}\nVariables:\n".format(script))
    for arg in vars(args_namespace):
        print("    [{}]: {}".format(arg, getattr(args, arg)))
    sys.stdout.write("\n")


def gb_to_fna(genome_dir, gb_file):
    fna_file = os.path.splitext(gb_file)[0] + ".fna"
    if not os.path.isfile(os.path.join(args.genome_dir, fna_file)):
        input_handle  = open(os.path.join(genome_dir, gb_file), "r")
        output_handle = open(os.path.join(genome_dir, fna_file), "w")
        sys.stdout.write("\t[GB_TO_FNA] Converting {} to {} in {}".format(gb_file, fna_file, genome_dir))
        sys.stdout.flush()
        for seq_record in SeqIO.parse(input_handle, "genbank") :
            output_handle.write(">{} {}\n{}\n".format(seq_record.id, seq_record.description, str(seq_record.seq)))
            input_handle.close()
            output_handle.close()
    else:
        sys.stdout.write("\t[GB_TO_FNA] FNA file {} exists".format(fna_file))
        sys.stdout.flush()

def get_genome_length(genome_dir, genome):
    genome_length = 0
    input_handle  = open(os.path.join(genome_dir, fna_file), "r")
    for seq_record in SeqIO.parse(input_handle, "fasta"):
        genome_length += len(seq_record)
    # Convert to Mb
    genome_length = genome_length / 1000000
    input_handle.close()
    return genome_length

def count_kmers(kmer_file):
    sys.stdout.write("\t[COUNT_KMERS] Counting KMERS in: {}\n".format(kmer_file))
    sys.stdout.flush()
    total_kmers = 0
    fasta_lines = len(open(kmer_file).readlines(  ))
    total_kmers = fasta_lines / 2
    return int(total_kmers)

def read_bam(bowtie_res):
    sys.stdout.write("\t[READ_BAM] Counting mapped reads from: {}\n".format(bowtie_res))
    sys.stdout.flush()
    unique_hits = 0
#    samfile = pysam.AlignmentFile(bowtie_res, "rb")
    if os.path.isfile(bowtie_res):
        mapped_reads = pysam.view("-c", "-F4", bowtie_res)
    else:
        sys.exit("[ERROR] {} does not exist!".format(bowtie_res))
    return int(mapped_reads)

def write_igs_dict(table, path, db):
    sys.stdout.write("\t[WRITE_IGS] Writing IGS/genome uniqueness table for DB: {} to: {}\n".format(db, path))
    sys.stdout.flush()
    output_handle = open(path, "w")
    first = True
    kmers = list()
    for genome in table:
        if first:
            kmers = sorted(list(table[genome].keys()))
            output_handle.write("genome\t{}\n".format("\t".join(kmers)))
            first = False
        output_handle.write("{}".format(genome))
        for kmer in kmers:
            output_handle.write("\t{}".format(table[genome][kmer]))
        output_handle.write("\n")
    output_handle.close()

def check_command(command):
    if not (shutil.which(command) is not None):
    	sys.exit("[ERROR] " + command + " is not available!")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mapping_file", help="[STRING] Input mapping file", dest="mapping_file", required=True)
    parser.add_argument("-g", "--genomes_directory", help="[STRING] Genome directory", dest="genome_dir", required=True)
    parser.add_argument("-k", "--kmers", help="[STRING] kmers list (single or comma-separated)", dest="kmer_list", required=True)
    parser.add_argument("-n", "--number_mismatches", help="[INTEGER] Allowable mismatches in mapping", dest="mismatches", default=0, type=int)
    args = parser.parse_args()

    report_call(sys.argv[0], args)

    kmer_array = sorted(args.kmer_list.split(','))

    mapping_lines = open( args.mapping_file, 'r').readlines()
    first = True
    headers = list()
    databases = dict()
    for line in mapping_lines:
        if line.startswith('#'):
            continue
        else:
            stripline = line.strip()
            splitline = stripline.split('\t')
            if (first):
                headers = splitline
                first = False
            else:
                sample_id = splitline[headers.index('SID')]
                genome_string = splitline[headers.index('GENOMES')]
                genome_set = genome_string.split(',')
#                genome_set_sort = sorted(genome_set.split(','))
                genome_set_format = set()

                for genome in genome_set:
                    if not (os.path.isfile(os.path.join(args.genome_dir, genome))):
                        sys.exit("[ERROR] Genome not found: " + genome)
                    if os.path.splitext(genome)[1] == ".gb" or os.path.splitext(genome)[1] == ".gbk":
                        fna_file = gb_to_fna(genome_dir, genome)
                        genome_set_format.add(fna_file)
                    elif os.path.splitext(genome)[1] == ".fna":
                        genome_set_format.add(genome)
                    else:
                        sys.exit("[ERROR] Incompatible file format detected for: " + genome)
                db_name = ','.join(sorted(genome_set_format))
#                sys.stdout.write(db_name + "\n")
                db_hash = hashlib.md5(db_name.encode('utf-8')).hexdigest()
#                sys.stdout.write(db_hash + "\n")
#                sys.stdout.flush()

                if not (genome_string in databases):
                    databases[genome_string] = db_hash
#                    db_path = os.path.join(args.genome_dir, db_hash + ".fna")
                    igs_path = os.path.join(args.genome_dir, db_hash + ".igs")
                    uniqueness_path = os.path.join(args.genome_dir, db_hash + ".genome_uniqueness")
#                    create_database(args.genome_dir, genome_set_format, db_path)
#                    index_database(db_path)

                    if os.path.isfile(igs_path):
                        existing_igs = open(igs_path, "r")
                        header = existing_igs.readline().strip()
#                        sys.stdout.write(header)
                        match = "genome\t" + "\t".join(kmer_array)
#                        sys.stdout.write(match)
                        if header == match:
                            sys.stdout.write("[STATUS] Existing IGS table validated for " + db_hash + " found!\n")
                            sys.stdout.flush()
                        else:
                            sys.stdout.write("[STATUS] Existing IGS table invalid for " + db_hash + ". Please delete and re-run!\n")
                            sys.stdout.flush()
                    else:
                        sys.stdout.write("[STATUS] Calculating IGS for genomes in database: " + db_hash + "\n")
                        sys.stdout.flush()
                        igs = dict()
                        uniqueness = dict()
                        for genome in genome_set_format:
                            igs[genome] = dict()
                            uniqueness[genome] = dict()
                            for kmer_length in kmer_array:
                                kmer_file = os.path.join(args.genome_dir, os.path.splitext(genome)[0] + "_" + kmer_length + "bp.kmers")
                                output_file_base = os.path.splitext(kmer_file)[0] + "_" + db_hash
                                bowtie_res = output_file_base + "_v" + str(args.mismatches) + ".bam"
#                                igs_dict[genome]["genome_length"] = get_genome_length(arg.genome_dir, genome)
                                all = count_kmers(kmer_file)
                                full_length = (all + (int(kmer_length) + 1)) / 1000000
                                hit = read_bam(bowtie_res)
                                percentage = hit / all
                                igs_val = full_length * percentage
                                igs[genome][kmer_length] = igs_val
                                uniqueness[genome][kmer_length] = percentage
                        write_igs_dict(igs, igs_path, db_hash)
                        write_igs_dict(uniqueness, uniqueness_path, db_hash)
