#!/usr/bin/env python3

import argparse
import sys
import os
import subprocess
import shutil
import hashlib

from Bio import SeqIO
from Bio import Seq

def assemble_from_kmers(fasta, distance):
    input_handle  = open(fasta, "r")
    unique_kmers = dict()
    for seq_record in SeqIO.parse(input_handle, "fasta") :
        id_split = seq_record.id.split('_')
        kmer_end_coord = int(id_split.pop())
        kmer_start_coord = int(id_split.pop())
        id_no_coord = "_".join(id_split)
        if not id_no_coord in unique_kmers:
            unique_kmers[id_no_coord] = dict()
            unique_kmers[id_no_coord][kmer_start_coord] = str(seq_record.seq)
        else:
            unique_kmers[id_no_coord][kmer_start_coord] = str(seq_record.seq)

    for id in unique_kmers:
        first = True
        prev = 0
        assembly = dict()
        seq_string = str()
        seq_string_start_coord = int()
        seq_string_end_coord = int()
        for start_coord in sorted(unique_kmers[id].keys()):
            if first:
                seq_string_start_coord = start_coord
                first = False

            if int(start_coord) - prev <= args.distance:
                seq_string = seq_string[0:(start_coord-seq_string_start_coord)] + unique_kmers[id][start_coord]
                seq_string_end_coord += int(start_coord) - prev
                prev = int(start_coord)
            else:
                # Write out previous subassembly set
                if prev != 0:
                    if not id in assembly:
                        assembly[id] = dict()
                    assembly[id][str(seq_string_start_coord) + "_" + str(seq_string_end_coord)] = seq_string
#                    sys.stdout.write("{} {}\n{}\n".format(id, str(seq_string_start_coord) + "_" + str(seq_string_end_coord), seq_string))
#                    sys.stdout.flush()

                # Begin new subassembly
                seq_string_start_coord = int(start_coord)
                seq_string = unique_kmers[id][start_coord]
                seq_string_end_coord = seq_string_start_coord + len(seq_string)
                prev = int(start_coord)
        if not id in assembly:
            assembly[id] = dict()
        assembly[id][str(seq_string_start_coord) + "_" + str(seq_string_end_coord)] = seq_string
#        sys.stdout.write("{} {}\n{}\n".format(id, str(seq_string_start_coord) + "_" + str(seq_string_end_coord), seq_string))
#        sys.stdout.flush()
        seq_string_start_coord = int(start_coord)
        seq_string = unique_kmers[id][start_coord]
        seq_string_end_coord = seq_string_start_coord + len(seq_string)

    input_handle.close()
    return assembly

def check_command(command):
    if not (shutil.which(command) is not None):
    	sys.exit("[ERROR] " + command + " is not available!")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-k", "--kmers_file", help="[STRING] unique kmers fasta", dest="kmer_file", required=True)
    parser.add_argument("-d", "--kmer_start_distance", help="[INT] distance between kmer start for joining", dest="distance", required=True, type=int)
    parser.add_argument("-o", "--outfile", help="[STRING] output file", dest="outfile", required=True)
    parser.add_argument("-m", "--min_length", help="[INT] minimum subassembly length for output", dest="min_length", type=int, default=0)
    args = parser.parse_args()

    if os.path.isfile(args.kmer_file):
        assembly = assemble_from_kmers(args.kmer_file, args.distance)
    else:
        sys.exit("[ERROR] Unable to find kmer input fasta: " + args.kmer_file)

    final_subassembly_file = open(args.outfile, 'w')

    for id in assembly:
        for subassembly in assembly[id]:
            if len(assembly[id][subassembly]) >= args.min_length:
                final_subassembly_file.write(">{}_{}\n{}\n".format(id, subassembly, assembly[id][subassembly]))

    final_subassembly_file.close()
